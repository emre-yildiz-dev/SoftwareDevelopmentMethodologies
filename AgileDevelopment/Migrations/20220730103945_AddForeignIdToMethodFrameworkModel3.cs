﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AgileDevelopment.Migrations
{
    public partial class AddForeignIdToMethodFrameworkModel3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Member_Methodology_MethodologyId",
                table: "Member");

            migrationBuilder.DropForeignKey(
                name: "FK_Practice_Methodology_MethodologyId",
                table: "Practice");

            migrationBuilder.DropForeignKey(
                name: "FK_Principle_Methodology_MethodologyId",
                table: "Principle");

            migrationBuilder.DropForeignKey(
                name: "FK_Test_Methodology_MethodologyId",
                table: "Test");

            migrationBuilder.DropForeignKey(
                name: "FK_WiseSaying_Methodology_MethodologyId",
                table: "WiseSaying");

            migrationBuilder.RenameColumn(
                name: "MethodologyId",
                table: "WiseSaying",
                newName: "MethodologyID");

            migrationBuilder.RenameIndex(
                name: "IX_WiseSaying_MethodologyId",
                table: "WiseSaying",
                newName: "IX_WiseSaying_MethodologyID");

            migrationBuilder.RenameColumn(
                name: "MethodologyId",
                table: "Test",
                newName: "MethodologyID");

            migrationBuilder.RenameIndex(
                name: "IX_Test_MethodologyId",
                table: "Test",
                newName: "IX_Test_MethodologyID");

            migrationBuilder.RenameColumn(
                name: "MethodologyId",
                table: "Principle",
                newName: "MethodologyID");

            migrationBuilder.RenameIndex(
                name: "IX_Principle_MethodologyId",
                table: "Principle",
                newName: "IX_Principle_MethodologyID");

            migrationBuilder.RenameColumn(
                name: "MethodologyId",
                table: "Practice",
                newName: "MethodologyID");

            migrationBuilder.RenameIndex(
                name: "IX_Practice_MethodologyId",
                table: "Practice",
                newName: "IX_Practice_MethodologyID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Methodology",
                newName: "MethodologyID");

            migrationBuilder.RenameColumn(
                name: "MethodologyId",
                table: "Member",
                newName: "MethodologyID");

            migrationBuilder.RenameIndex(
                name: "IX_Member_MethodologyId",
                table: "Member",
                newName: "IX_Member_MethodologyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Member_Methodology_MethodologyID",
                table: "Member",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Practice_Methodology_MethodologyID",
                table: "Practice",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Principle_Methodology_MethodologyID",
                table: "Principle",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Test_Methodology_MethodologyID",
                table: "Test",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID");

            migrationBuilder.AddForeignKey(
                name: "FK_WiseSaying_Methodology_MethodologyID",
                table: "WiseSaying",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Member_Methodology_MethodologyID",
                table: "Member");

            migrationBuilder.DropForeignKey(
                name: "FK_Practice_Methodology_MethodologyID",
                table: "Practice");

            migrationBuilder.DropForeignKey(
                name: "FK_Principle_Methodology_MethodologyID",
                table: "Principle");

            migrationBuilder.DropForeignKey(
                name: "FK_Test_Methodology_MethodologyID",
                table: "Test");

            migrationBuilder.DropForeignKey(
                name: "FK_WiseSaying_Methodology_MethodologyID",
                table: "WiseSaying");

            migrationBuilder.RenameColumn(
                name: "MethodologyID",
                table: "WiseSaying",
                newName: "MethodologyId");

            migrationBuilder.RenameIndex(
                name: "IX_WiseSaying_MethodologyID",
                table: "WiseSaying",
                newName: "IX_WiseSaying_MethodologyId");

            migrationBuilder.RenameColumn(
                name: "MethodologyID",
                table: "Test",
                newName: "MethodologyId");

            migrationBuilder.RenameIndex(
                name: "IX_Test_MethodologyID",
                table: "Test",
                newName: "IX_Test_MethodologyId");

            migrationBuilder.RenameColumn(
                name: "MethodologyID",
                table: "Principle",
                newName: "MethodologyId");

            migrationBuilder.RenameIndex(
                name: "IX_Principle_MethodologyID",
                table: "Principle",
                newName: "IX_Principle_MethodologyId");

            migrationBuilder.RenameColumn(
                name: "MethodologyID",
                table: "Practice",
                newName: "MethodologyId");

            migrationBuilder.RenameIndex(
                name: "IX_Practice_MethodologyID",
                table: "Practice",
                newName: "IX_Practice_MethodologyId");

            migrationBuilder.RenameColumn(
                name: "MethodologyID",
                table: "Methodology",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "MethodologyID",
                table: "Member",
                newName: "MethodologyId");

            migrationBuilder.RenameIndex(
                name: "IX_Member_MethodologyID",
                table: "Member",
                newName: "IX_Member_MethodologyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Member_Methodology_MethodologyId",
                table: "Member",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Practice_Methodology_MethodologyId",
                table: "Practice",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Principle_Methodology_MethodologyId",
                table: "Principle",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Test_Methodology_MethodologyId",
                table: "Test",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_WiseSaying_Methodology_MethodologyId",
                table: "WiseSaying",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id");
        }
    }
}
