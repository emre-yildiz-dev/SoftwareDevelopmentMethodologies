﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AgileDevelopment.Migrations
{
    public partial class ModelIDHandleForViewData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Member_Methodology_MethodologyID",
                table: "Member");

            migrationBuilder.DropForeignKey(
                name: "FK_MindSet_Member_MemberId",
                table: "MindSet");

            migrationBuilder.DropForeignKey(
                name: "FK_Practice_Methodology_MethodologyID",
                table: "Practice");

            migrationBuilder.DropForeignKey(
                name: "FK_Principle_Methodology_MethodologyID",
                table: "Principle");

            migrationBuilder.DropForeignKey(
                name: "FK_Test_Methodology_MethodologyID",
                table: "Test");

            migrationBuilder.DropForeignKey(
                name: "FK_WiseSaying_Methodology_MethodologyID",
                table: "WiseSaying");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "WiseSaying",
                newName: "WiseSayingID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Test",
                newName: "TestID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Principle",
                newName: "PrincipleID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Practice",
                newName: "PracticeID");

            migrationBuilder.RenameColumn(
                name: "MemberId",
                table: "MindSet",
                newName: "MemberID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "MindSet",
                newName: "MindSetID");

            migrationBuilder.RenameIndex(
                name: "IX_MindSet_MemberId",
                table: "MindSet",
                newName: "IX_MindSet_MemberID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "MethodFrameworks",
                newName: "MethodFrameworkID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Member",
                newName: "MemberID");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyID",
                table: "WiseSaying",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyID",
                table: "Test",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyID",
                table: "Principle",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyID",
                table: "Practice",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MemberID",
                table: "MindSet",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyID",
                table: "Member",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Member_Methodology_MethodologyID",
                table: "Member",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MindSet_Member_MemberID",
                table: "MindSet",
                column: "MemberID",
                principalTable: "Member",
                principalColumn: "MemberID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Practice_Methodology_MethodologyID",
                table: "Practice",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Principle_Methodology_MethodologyID",
                table: "Principle",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Test_Methodology_MethodologyID",
                table: "Test",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WiseSaying_Methodology_MethodologyID",
                table: "WiseSaying",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Member_Methodology_MethodologyID",
                table: "Member");

            migrationBuilder.DropForeignKey(
                name: "FK_MindSet_Member_MemberID",
                table: "MindSet");

            migrationBuilder.DropForeignKey(
                name: "FK_Practice_Methodology_MethodologyID",
                table: "Practice");

            migrationBuilder.DropForeignKey(
                name: "FK_Principle_Methodology_MethodologyID",
                table: "Principle");

            migrationBuilder.DropForeignKey(
                name: "FK_Test_Methodology_MethodologyID",
                table: "Test");

            migrationBuilder.DropForeignKey(
                name: "FK_WiseSaying_Methodology_MethodologyID",
                table: "WiseSaying");

            migrationBuilder.RenameColumn(
                name: "WiseSayingID",
                table: "WiseSaying",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "TestID",
                table: "Test",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "PrincipleID",
                table: "Principle",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "PracticeID",
                table: "Practice",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "MemberID",
                table: "MindSet",
                newName: "MemberId");

            migrationBuilder.RenameColumn(
                name: "MindSetID",
                table: "MindSet",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_MindSet_MemberID",
                table: "MindSet",
                newName: "IX_MindSet_MemberId");

            migrationBuilder.RenameColumn(
                name: "MethodFrameworkID",
                table: "MethodFrameworks",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "MemberID",
                table: "Member",
                newName: "Id");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyID",
                table: "WiseSaying",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyID",
                table: "Test",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyID",
                table: "Principle",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyID",
                table: "Practice",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "MemberId",
                table: "MindSet",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyID",
                table: "Member",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Member_Methodology_MethodologyID",
                table: "Member",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID");

            migrationBuilder.AddForeignKey(
                name: "FK_MindSet_Member_MemberId",
                table: "MindSet",
                column: "MemberId",
                principalTable: "Member",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Practice_Methodology_MethodologyID",
                table: "Practice",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Principle_Methodology_MethodologyID",
                table: "Principle",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Test_Methodology_MethodologyID",
                table: "Test",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID");

            migrationBuilder.AddForeignKey(
                name: "FK_WiseSaying_Methodology_MethodologyID",
                table: "WiseSaying",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "MethodologyID");
        }
    }
}
