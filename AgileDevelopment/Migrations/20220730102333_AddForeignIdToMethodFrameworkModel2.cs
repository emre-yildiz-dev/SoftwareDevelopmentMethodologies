﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AgileDevelopment.Migrations
{
    public partial class AddForeignIdToMethodFrameworkModel2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MethodFrameworks_Methodology_MethodologyId",
                table: "MethodFrameworks");

            migrationBuilder.RenameColumn(
                name: "MethodologyId",
                table: "MethodFrameworks",
                newName: "MethodologyID");

            migrationBuilder.RenameIndex(
                name: "IX_MethodFrameworks_MethodologyId",
                table: "MethodFrameworks",
                newName: "IX_MethodFrameworks_MethodologyID");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyID",
                table: "MethodFrameworks",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MethodFrameworks_Methodology_MethodologyID",
                table: "MethodFrameworks",
                column: "MethodologyID",
                principalTable: "Methodology",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MethodFrameworks_Methodology_MethodologyID",
                table: "MethodFrameworks");

            migrationBuilder.RenameColumn(
                name: "MethodologyID",
                table: "MethodFrameworks",
                newName: "MethodologyId");

            migrationBuilder.RenameIndex(
                name: "IX_MethodFrameworks_MethodologyID",
                table: "MethodFrameworks",
                newName: "IX_MethodFrameworks_MethodologyId");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "MethodFrameworks",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_MethodFrameworks_Methodology_MethodologyId",
                table: "MethodFrameworks",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id");
        }
    }
}
