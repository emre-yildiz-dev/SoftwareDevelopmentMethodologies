﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AgileDevelopment.Migrations
{
    public partial class HandleNullValuesInModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Member_Methodology_MethodologyId",
                table: "Member");

            migrationBuilder.DropForeignKey(
                name: "FK_MethodFrameworks_Methodology_MethodologyId",
                table: "MethodFrameworks");

            migrationBuilder.DropForeignKey(
                name: "FK_MindSet_Member_MemberId",
                table: "MindSet");

            migrationBuilder.DropForeignKey(
                name: "FK_Practice_Methodology_MethodologyId",
                table: "Practice");

            migrationBuilder.DropForeignKey(
                name: "FK_Principle_Methodology_MethodologyId",
                table: "Principle");

            migrationBuilder.DropForeignKey(
                name: "FK_Test_Methodology_MethodologyId",
                table: "Test");

            migrationBuilder.DropForeignKey(
                name: "FK_WiseSaying_Methodology_MethodologyId",
                table: "WiseSaying");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "WiseSaying",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "Test",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "Principle",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "Practice",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "MemberId",
                table: "MindSet",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "MethodFrameworks",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "Member",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Member_Methodology_MethodologyId",
                table: "Member",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MethodFrameworks_Methodology_MethodologyId",
                table: "MethodFrameworks",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MindSet_Member_MemberId",
                table: "MindSet",
                column: "MemberId",
                principalTable: "Member",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Practice_Methodology_MethodologyId",
                table: "Practice",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Principle_Methodology_MethodologyId",
                table: "Principle",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Test_Methodology_MethodologyId",
                table: "Test",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_WiseSaying_Methodology_MethodologyId",
                table: "WiseSaying",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Member_Methodology_MethodologyId",
                table: "Member");

            migrationBuilder.DropForeignKey(
                name: "FK_MethodFrameworks_Methodology_MethodologyId",
                table: "MethodFrameworks");

            migrationBuilder.DropForeignKey(
                name: "FK_MindSet_Member_MemberId",
                table: "MindSet");

            migrationBuilder.DropForeignKey(
                name: "FK_Practice_Methodology_MethodologyId",
                table: "Practice");

            migrationBuilder.DropForeignKey(
                name: "FK_Principle_Methodology_MethodologyId",
                table: "Principle");

            migrationBuilder.DropForeignKey(
                name: "FK_Test_Methodology_MethodologyId",
                table: "Test");

            migrationBuilder.DropForeignKey(
                name: "FK_WiseSaying_Methodology_MethodologyId",
                table: "WiseSaying");

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "WiseSaying",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "Test",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "Principle",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "Practice",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MemberId",
                table: "MindSet",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "MethodFrameworks",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MethodologyId",
                table: "Member",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Member_Methodology_MethodologyId",
                table: "Member",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MethodFrameworks_Methodology_MethodologyId",
                table: "MethodFrameworks",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MindSet_Member_MemberId",
                table: "MindSet",
                column: "MemberId",
                principalTable: "Member",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Practice_Methodology_MethodologyId",
                table: "Practice",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Principle_Methodology_MethodologyId",
                table: "Principle",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Test_Methodology_MethodologyId",
                table: "Test",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WiseSaying_Methodology_MethodologyId",
                table: "WiseSaying",
                column: "MethodologyId",
                principalTable: "Methodology",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
